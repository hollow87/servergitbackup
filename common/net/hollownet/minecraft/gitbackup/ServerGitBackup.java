package net.hollownet.minecraft.gitbackup;

import java.io.File;

import net.hollownet.minecraft.gitbackup.command.CommandHandler;
import net.hollownet.minecraft.gitbackup.configuration.ConfigurationHandler;
import net.hollownet.minecraft.gitbackup.core.helper.LogHelper;
import net.hollownet.minecraft.gitbackup.lib.Reference;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;

/**
 * ServerGitBackup
 * 
 * ServerGitBackup
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME)
public class ServerGitBackup {

	@Instance(Reference.MOD_ID)
	public static ServerGitBackup instance;

	@EventHandler
	public void preInt(FMLPreInitializationEvent event) {
		LogHelper.init();

		ConfigurationHandler.init(event.getModConfigurationDirectory()
				.getAbsolutePath()
				+ File.separator
				+ Reference.CHANNEL_NAME.toLowerCase() + File.separator);
	}

	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) {
		CommandHandler.initCommands(event);

	}
}