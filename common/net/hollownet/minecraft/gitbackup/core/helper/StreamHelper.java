package net.hollownet.minecraft.gitbackup.core.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;

import net.hollownet.minecraft.gitbackup.lib.Reference;
import cpw.mods.fml.common.FMLLog;

/**
 * ServerGitBackup
 * 
 * StreamHelper
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class StreamHelper extends Thread {

	private InputStream is = null;
	private boolean os = false;

	public StreamHelper(InputStream is) {
		this(is, false);
	}

	public StreamHelper(InputStream is, boolean redirect) {
		this.is = is;
		this.os = redirect;
	}

	@Override
	public void run() {
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);

		try {
			String line = null;

			while ((line = br.readLine()) != null) {
				if (os)
					LogHelper.debug(line);
			}
		} catch (IOException e) {
			FMLLog.log(Level.WARNING, e, Reference.MOD_NAME
					+ " has had a problem with reading the IO streams");
		} finally {
			try {
				if (isr != null)
					isr.close();
			} catch (IOException e) {
				FMLLog.log(Level.WARNING, e, Reference.MOD_NAME
						+ " has had a problem with reading the IO streams");
			}

			try {
				if (br != null)
					br.close();
			} catch (IOException e) {
				FMLLog.log(Level.WARNING, e, Reference.MOD_NAME
						+ " has had a problem with reading the IO streams");
			}

		}
	}
}
