package net.hollownet.minecraft.gitbackup.core.helper;

import java.util.logging.Level;
import java.util.logging.Logger;

import net.hollownet.minecraft.gitbackup.configuration.ConfigurationSettings;
import net.hollownet.minecraft.gitbackup.lib.Reference;
import cpw.mods.fml.common.FMLLog;

/**
 * Equivalent-Exchange-3
 * 
 * LogHelper
 * 
 * 
 * @author pahimar
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 *          Modified by hollow87 for ServerGitBackup
 */
public class LogHelper {

	private static Logger sgbLogger = Logger.getLogger(Reference.MOD_ID);

	public static void init() {
		sgbLogger.setParent(FMLLog.getLogger());
	}

	public static void log(Level logLevel, Object object) {
		sgbLogger.log(logLevel, object.toString());
	}

	public static void severe(Object object) {
		log(Level.SEVERE, object.toString());
	}

	public static void debug(Object object) {
		if (ConfigurationSettings.DEBUG) {
			log(Level.WARNING, "[DEBUG] " + object.toString());
		}
	}

	public static void warning(Object object) {
		log(Level.WARNING, object.toString());
	}

	public static void info(Object object) {
		log(Level.INFO, object.toString());
	}

	public static void config(Object object) {
		log(Level.CONFIG, object.toString());
	}

	public static void fine(Object object) {
		log(Level.FINE, object.toString());
	}

	public static void finer(Object object) {
		log(Level.FINER, object.toString());
	}

	public static void finest(Object object) {
		log(Level.FINEST, object.toString());
	}
}
