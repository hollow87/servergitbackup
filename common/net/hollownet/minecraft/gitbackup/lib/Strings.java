package net.hollownet.minecraft.gitbackup.lib;

/**
 * ServerGitBackup
 * 
 * Strings
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class Strings {

	public static final String COMMAND_SAY = "say";
	public static final String COMMAND_SAVEOFF = "save-off";
	public static final String COMMAND_SAVEALL = "save-all";
	public static final String COMMAND_SAVEON = "save-on";

	public static final String BACKUP_READONLY = "Backup started server going read-only";
	public static final String BACKUP_READWRITE = "Backup complete server going read-write";

	public static final String GIT_INIT = "Initialising git repo";
	public static final String GIT_INIT_DONE = "Initialization done";
	public static final String GIT_ADD = "Adding files to git repo";
	public static final String GIT_COMMIT = "Commiting to git repo";

	public static final String AUTOBACKUP_ON = "Auto-backup on";
	public static final String AUTOBACKUP_OFF = "Auto-backup off";
}
