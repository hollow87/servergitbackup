package net.hollownet.minecraft.gitbackup.lib;

/**
 * ServerGitBackup
 * 
 * Commands
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class Commands {

	public static final String COMMAND_ON = "on";
	public static final String COMMAND_OFF = "off";

	public static final String COMMAND_SGB = "sgb";
	public static final String COMMAND_SGB_USAGE = "sgb [init | backup | autobackup]";

	public static final String COMMAND_BACKUP = "backup";
	public static final String COMMAND_INIT = "init";

	public static final String COMMAND_AUTOBACKUP = "autobackup";
	public static final String COMMAND_AUTOBACKUP_USAGE = "sgb autobackup [on | off]";

}
