package net.hollownet.minecraft.gitbackup.lib;

/**
 * ServerGitBackup
 * 
 * Reference
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class Reference {
	public static final String MOD_ID = "servergitbackup";
	public static final String MOD_NAME = "Server Git Backup";
	public static final String CHANNEL_NAME = "SGB";
}
