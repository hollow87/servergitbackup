package net.hollownet.minecraft.gitbackup.configuration;

import java.io.File;

import net.minecraftforge.common.Configuration;

/**
 * ServerGitBackup
 * 
 * ConfigurationHandler
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class ConfigurationHandler {

	public static Configuration configuration;

	/**
	 * 
	 * @param configPath
	 *            A path to the config directory
	 */
	public static void init(String configPath) {
		GeneralConfiguration.init(new File(configPath + "general.properties"));
	}

}
