package net.hollownet.minecraft.gitbackup.configuration;

import static net.minecraftforge.common.Configuration.CATEGORY_GENERAL;

import java.io.File;
import java.util.logging.Level;

import net.hollownet.minecraft.gitbackup.lib.Reference;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.common.FMLLog;

/**
 * ServerGitBackup
 * 
 * GeneralConfiguration
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class GeneralConfiguration {

	private static Configuration generalConfiguration;

	protected static void init(File configFile) {
		generalConfiguration = new Configuration(configFile);

		try {
			generalConfiguration.load();

			ConfigurationSettings.AUTOBACKUP_TIMER = generalConfiguration.get(
					CATEGORY_GENERAL,
					ConfigurationSettings.AUTOBACKUP_TIMER_CONFIGNAME,
					ConfigurationSettings.AUTOBACKUP_TIMER_DEFAULT).getInt(
					ConfigurationSettings.AUTOBACKUP_TIMER);

			ConfigurationSettings.DEBUG = generalConfiguration.get(
					CATEGORY_GENERAL, ConfigurationSettings.DEBUG_CONFIGNAME,
					ConfigurationSettings.DEBUG_DEFAULT).getBoolean(
					ConfigurationSettings.DEBUG_DEFAULT);
		} catch (Exception e) {
			FMLLog.log(Level.SEVERE, e, Reference.MOD_NAME
					+ " has had a problem loading its general configuration");
		} finally {
			generalConfiguration.save();
		}
	}

	public static void set(String categoryName, String propertyName,
			String newValue) {

		generalConfiguration.load();
		if (generalConfiguration.getCategoryNames().contains(categoryName)) {
			if (generalConfiguration.getCategory(categoryName).containsKey(
					propertyName)) {
				generalConfiguration.getCategory(categoryName)
						.get(propertyName).set(newValue);
			}
		}
		generalConfiguration.save();
	}
}
