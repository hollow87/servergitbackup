package net.hollownet.minecraft.gitbackup.configuration;

/**
 * ServerGitBackup
 * 
 * ConfigurationSettings
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class ConfigurationSettings {

	public static int AUTOBACKUP_TIMER;
	public static final String AUTOBACKUP_TIMER_CONFIGNAME = "autobackup.timer";
	public static final int AUTOBACKUP_TIMER_DEFAULT = 60;

	public static boolean DEBUG;
	public static final String DEBUG_CONFIGNAME = "debug";
	public static final boolean DEBUG_DEFAULT = false;
}
