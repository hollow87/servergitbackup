package net.hollownet.minecraft.gitbackup.command;

import java.io.IOException;

import net.hollownet.minecraft.gitbackup.configuration.ConfigurationSettings;
import net.hollownet.minecraft.gitbackup.core.helper.StreamHelper;
import net.hollownet.minecraft.gitbackup.lib.Commands;
import net.hollownet.minecraft.gitbackup.lib.Strings;
import net.minecraft.command.ICommandManager;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.world.World;

/**
 * ServerGitBackup
 * 
 * CommandInit
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class CommandInit implements ICommandSender {

	public static void processCommand(ICommandSender commandSender,
			String[] args) {
		String subCommand;

		if (args.length > 0) {
			subCommand = args[0];

			if (subCommand.toLowerCase().equals(Commands.COMMAND_INIT)) {
				processInitCommand(commandSender);
			}
		}
	}

	private static void processInitCommand(ICommandSender commandSender) {
		CommandBackup sender = new CommandBackup();
		MinecraftServer server = MinecraftServer.getServer();
		ICommandManager commandManager = server.getCommandManager();

		commandManager.executeCommand(sender, Strings.COMMAND_SAY + " "
				+ Strings.GIT_INIT);

		Runnable gitInit = new Runnable() {

			@Override
			public void run() {
				Process p = null;

				CommandBackup sender = new CommandBackup();
				MinecraftServer server = MinecraftServer.getServer();
				ICommandManager commandManager = server.getCommandManager();

				StreamHelper stdErr = null;
				StreamHelper stdOut = null;

				ProcessBuilder pb = new ProcessBuilder("git", "init");
				pb.directory(server.getFile(""));

				try {
					p = pb.start();

					if (ConfigurationSettings.DEBUG) {
						stdErr = new StreamHelper(p.getErrorStream(), true);
						stdOut = new StreamHelper(p.getInputStream(), true);
					} else {
						stdErr = new StreamHelper(p.getErrorStream(), false);
						stdOut = new StreamHelper(p.getInputStream(), false);
					}

					stdErr.start();
					stdOut.start();

					p.waitFor();

					commandManager.executeCommand(sender, Strings.COMMAND_SAY
							+ " " + Strings.GIT_INIT_DONE);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		};

		gitInit.run();

	}

	@Override
	public String getCommandSenderName() {
		// TODO Auto-generated method stub
		return "Server";
	}

	@Override
	public void sendChatToPlayer(ChatMessageComponent chatmessagecomponent) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canCommandSenderUseCommand(int i, String s) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ChunkCoordinates getPlayerCoordinates() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public World getEntityWorld() {
		// TODO Auto-generated method stub
		return null;
	}

}
