package net.hollownet.minecraft.gitbackup.command;

import net.hollownet.minecraft.gitbackup.lib.Commands;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;

/**
 * ServerGitBackup
 * 
 * CommandSGB
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class CommandSGB extends CommandBase {

	@Override
	public String getCommandName() {
		return Commands.COMMAND_SGB;
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 4;
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender) {
		return null;
	}

	@Override
	public void processCommand(ICommandSender commandSender, String[] args) {
		if (args.length > 0) {
			String commandName = args[0];
			System.arraycopy(args, 1, args, 0, args.length - 1);

			if (commandName.equalsIgnoreCase(Commands.COMMAND_BACKUP)) {
				CommandBackup.processCommand(commandSender, args);
			} else if (commandName.equalsIgnoreCase(Commands.COMMAND_INIT)) {
				CommandInit.processCommand(commandSender, args);
			} else if (commandName
					.equalsIgnoreCase(Commands.COMMAND_AUTOBACKUP)) {
				CommandAutoBackup.processCommand(commandSender, args);
			} else
				throw new WrongUsageException(Commands.COMMAND_SGB_USAGE,
						new Object[0]);
		} else
			throw new WrongUsageException(Commands.COMMAND_SGB_USAGE,
					new Object[0]);

	}

}
