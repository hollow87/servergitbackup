package net.hollownet.minecraft.gitbackup.command;

import cpw.mods.fml.common.event.FMLServerStartingEvent;

/**
 * ServerGitBackup
 * 
 * CommandHandler
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class CommandHandler {
	public static void initCommands(FMLServerStartingEvent event) {
		event.registerServerCommand(new CommandSGB());
	}
}
