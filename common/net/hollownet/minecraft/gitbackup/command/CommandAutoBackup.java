package net.hollownet.minecraft.gitbackup.command;

import java.util.Timer;
import java.util.TimerTask;

import net.hollownet.minecraft.gitbackup.configuration.ConfigurationSettings;
import net.hollownet.minecraft.gitbackup.lib.Commands;
import net.hollownet.minecraft.gitbackup.lib.Strings;
import net.minecraft.command.ICommandManager;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.world.World;

/**
 * ServerGitBackup
 * 
 * CommandAutoBackup
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class CommandAutoBackup implements ICommandSender {

	private static Timer timer = null;

	public static void processCommand(ICommandSender commandSender,
			String[] args) {
		String subCommand;

		if (args.length > 0) {
			subCommand = args[0];

			if (subCommand.toLowerCase().equals(Commands.COMMAND_ON)) {
				processOnCommand(commandSender);
			} else if (subCommand.toLowerCase().equals(Commands.COMMAND_OFF)) {
				processOffCommand(commandSender);
			} else
				throw new WrongUsageException(
						Commands.COMMAND_AUTOBACKUP_USAGE, new Object[0]);
		} else
			throw new WrongUsageException(Commands.COMMAND_AUTOBACKUP_USAGE,
					new Object[0]);
	}

	private static void processOnCommand(ICommandSender commandSender) {
		CommandBackup sender = new CommandBackup();
		MinecraftServer server = MinecraftServer.getServer();
		ICommandManager commandManager = server.getCommandManager();

		if (timer == null) {
			commandManager.executeCommand(sender, Strings.COMMAND_SAY + " "
					+ Strings.AUTOBACKUP_ON);

			timer = new Timer();

			timer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					CommandBackup.processCommand(null,
							new String[] { "backup" });
				}

			}, 0, ConfigurationSettings.AUTOBACKUP_TIMER * 60 * 1000);
		}
	}

	private static void processOffCommand(ICommandSender commandSender) {
		CommandBackup sender = new CommandBackup();
		MinecraftServer server = MinecraftServer.getServer();
		ICommandManager commandManager = server.getCommandManager();

		if (timer != null) {
			commandManager.executeCommand(sender, Strings.COMMAND_SAY + " "
					+ Strings.AUTOBACKUP_OFF);

			timer.cancel();
			timer = null;
		}
	}

	@Override
	public String getCommandSenderName() {
		// TODO Auto-generated method stub
		return "Server";
	}

	@Override
	public void sendChatToPlayer(ChatMessageComponent chatmessagecomponent) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canCommandSenderUseCommand(int i, String s) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ChunkCoordinates getPlayerCoordinates() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public World getEntityWorld() {
		// TODO Auto-generated method stub
		return null;
	}

}
