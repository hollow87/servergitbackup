package net.hollownet.minecraft.gitbackup.command;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;

import net.hollownet.minecraft.gitbackup.configuration.ConfigurationSettings;
import net.hollownet.minecraft.gitbackup.core.helper.StreamHelper;
import net.hollownet.minecraft.gitbackup.lib.Commands;
import net.hollownet.minecraft.gitbackup.lib.Reference;
import net.hollownet.minecraft.gitbackup.lib.Strings;
import net.minecraft.command.ICommandManager;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.world.World;
import cpw.mods.fml.common.FMLLog;

/**
 * ServerGitBackup
 * 
 * CommandBackup
 * 
 * @author hollow87
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class CommandBackup implements ICommandSender {

	public static void processCommand(ICommandSender commandSender,
			String[] args) {
		String subCommand;

		if (args.length > 0) {
			subCommand = args[0];

			if (subCommand.toLowerCase().equals(Commands.COMMAND_BACKUP)) {
				processBackupCommand(commandSender);
			}
		}
	}

	private static void processBackupCommand(ICommandSender commandSender) {
		CommandBackup sender = new CommandBackup();
		MinecraftServer server = MinecraftServer.getServer();
		ICommandManager commandManager = server.getCommandManager();

		commandManager.executeCommand(sender, Strings.COMMAND_SAY + " "
				+ Strings.BACKUP_READONLY);
		commandManager.executeCommand(sender, Strings.COMMAND_SAVEOFF);
		commandManager.executeCommand(sender, Strings.COMMAND_SAVEALL);

		Runnable gitAddCommit = new Runnable() {

			@Override
			public void run() {
				Process p = null;
				CommandBackup sender = new CommandBackup();
				MinecraftServer server = MinecraftServer.getServer();
				ICommandManager commandManager = server.getCommandManager();

				StreamHelper stdErr = null;
				StreamHelper stdOut = null;

				commandManager.executeCommand(sender, Strings.COMMAND_SAY + " "
						+ Strings.GIT_ADD);
				ProcessBuilder pb = new ProcessBuilder("git", "add", "-A",
						"--ignore-errors", "-f");
				pb.directory(server.getFile(""));

				try {

					p = pb.start();

					if (ConfigurationSettings.DEBUG) {
						stdErr = new StreamHelper(p.getErrorStream(), true);
						stdOut = new StreamHelper(p.getInputStream(), true);
					} else {
						stdErr = new StreamHelper(p.getErrorStream(), false);
						stdOut = new StreamHelper(p.getInputStream(), false);
					}

					stdErr.start();
					stdOut.start();

					p.waitFor();

					String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
							.format(Calendar.getInstance().getTime());

					commandManager.executeCommand(sender, Strings.COMMAND_SAY
							+ " " + Strings.GIT_COMMIT);
					pb = new ProcessBuilder("git", "commit", "-m", "\"Backup "
							+ timeStamp + "\"");

					pb.directory(server.getFile(""));

					p = pb.start();

					if (ConfigurationSettings.DEBUG) {
						stdErr = new StreamHelper(p.getErrorStream(), true);
						stdOut = new StreamHelper(p.getInputStream(), true);
					} else {
						stdErr = new StreamHelper(p.getErrorStream(), false);
						stdOut = new StreamHelper(p.getInputStream(), false);
					}

					stdErr.start();
					stdOut.start();

					p.waitFor();

					commandManager.executeCommand(sender, Strings.COMMAND_SAY
							+ " " + Strings.BACKUP_READWRITE);
					commandManager.executeCommand(sender,
							Strings.COMMAND_SAVEON);
				} catch (IOException e) {
					FMLLog.log(Level.WARNING, e, Reference.MOD_NAME
							+ " has had a problem running the backup commands");
				} catch (InterruptedException e) {
					FMLLog.log(Level.WARNING, e, Reference.MOD_NAME
							+ " has had its backup thread interrupted");
				}
			}

		};

		gitAddCommit.run();
	}

	@Override
	public String getCommandSenderName() {
		return "Server";
	}

	@Override
	public void sendChatToPlayer(ChatMessageComponent chatmessagecomponent) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canCommandSenderUseCommand(int i, String s) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public ChunkCoordinates getPlayerCoordinates() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public World getEntityWorld() {
		// TODO Auto-generated method stub
		return null;
	}
}
